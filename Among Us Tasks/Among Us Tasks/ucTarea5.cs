﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Among_Us_Tasks
{
    public partial class ucTarea5 : UserControl
    {
        public ucTarea5()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Button btnPresionado = (Button)sender;
            if (btnPresionado.Width > 25)
            {
                btnPresionado.Width -= 5;
                btnPresionado.Height -= 5;
                btnPresionado.Left += 3;
                btnPresionado.Top += 3;
            }
            else
            {
                btnPresionado.Visible = false;

                if (!button1.Visible && !button2.Visible && !button3.Visible && !button4.Visible)
                {
                    label1.Text = "Reparar Taladro Completado";
                    label1.ForeColor = Color.Lime;
                    lblStatus.Text = "OK";
                    lblStatus.ForeColor = Color.Lime;

                    frmMain.TareasCompletas[4] = true;
                }
            }
        }
    }
}
