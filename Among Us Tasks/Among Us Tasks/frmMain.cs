﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Among_Us_Tasks
{
    public partial class frmMain : Form
    {
        public static bool[] TareasCompletas = new bool[6];

        public frmMain()
        {
            InitializeComponent();
        }

        private void rbtnInicio_CheckedChanged(object sender, EventArgs e)
        {
            panelInicio.Visible = rbtnInicio.Checked;
        }

        private void rbtnTarea1_CheckedChanged(object sender, EventArgs e)
        {
            ucTarea11.Visible = rbtnTarea1.Checked;
        }

        private void rbtnTarea2_CheckedChanged(object sender, EventArgs e)
        {
            ucTarea21.Visible = rbtnTarea2.Checked;
        }

        private void rbtnTarea3_CheckedChanged(object sender, EventArgs e)
        {
            ucTarea31.Visible = rbtnTarea3.Checked;
        }

        private void rbtnTarea4_CheckedChanged(object sender, EventArgs e)
        {
            ucTarea41.Visible = rbtnTarea4.Checked;
        }

        private void rbtnTarea5_CheckedChanged(object sender, EventArgs e)
        {
            ucTarea51.Visible = rbtnTarea5.Checked;
        }

        private void rbtnTarea6_CheckedChanged(object sender, EventArgs e)
        {
            ucTarea61.Visible = rbtnTarea6.Checked;
        }

        private void tmrVerificarTareas_Tick(object sender, EventArgs e)
        {
            if (TareasCompletas[0] == true)
            {
                rbtnTarea1.BackColor = Color.LimeGreen;
                rbtnTarea1.FlatAppearance.CheckedBackColor = Color.LimeGreen;
            }
            if (TareasCompletas[1] == true)
            {
                rbtnTarea2.BackColor = Color.LimeGreen;
                rbtnTarea2.FlatAppearance.CheckedBackColor = Color.LimeGreen;
            }
            if (TareasCompletas[2] == true)
            {
                rbtnTarea3.BackColor = Color.LimeGreen;
                rbtnTarea3.FlatAppearance.CheckedBackColor = Color.LimeGreen;
            }
            if (TareasCompletas[3] == true)
            {
                rbtnTarea4.BackColor = Color.LimeGreen;
                rbtnTarea4.FlatAppearance.CheckedBackColor = Color.LimeGreen;
            }
            if (TareasCompletas[4] == true)
            {
                rbtnTarea5.BackColor = Color.LimeGreen;
                rbtnTarea5.FlatAppearance.CheckedBackColor = Color.LimeGreen;
            }
            if (TareasCompletas[5])
            {
                rbtnTarea6.BackColor = Color.LimeGreen;
                rbtnTarea6.FlatAppearance.CheckedBackColor = Color.LimeGreen;
            }

            if (TareasCompletas[0] && TareasCompletas[1] &&
                TareasCompletas[2] && TareasCompletas[3] &&
                TareasCompletas[4] && TareasCompletas[5])
            {
                tmrVerificarTareas.Enabled = false;
                MessageBox.Show("Tareas completas exitosamente.");
            }
        }
    }
}
