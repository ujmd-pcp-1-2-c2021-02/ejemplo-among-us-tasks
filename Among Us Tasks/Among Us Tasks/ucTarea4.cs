﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Among_Us_Tasks
{
    public partial class ucTarea4 : UserControl
    {
        public ucTarea4()
        {
            InitializeComponent();
        }

        private void ucTarea4_Load(object sender, EventArgs e)
        {
            lblIDDigitado.Text = "";

            Random rnd = new Random();
            int ID = rnd.Next(0, 100000);
            lblIDTarjeta.Text = ID.ToString("00000");
        }

        private void panelTarjetaBilletera_Click(object sender, EventArgs e)
        {
            panelTarjeta.Visible = true;
        }

        private void button0_Click(object sender, EventArgs e)
        {
            Button btnPresionado = (Button)sender;

            if (lblIDDigitado.Text.Length < 5)
            {
                lblIDDigitado.Text += btnPresionado.Text;
            }
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            lblIDDigitado.Text = "";
        }

        private void btnAcpetar_Click(object sender, EventArgs e)
        {
            if (lblIDDigitado.Text == lblIDTarjeta.Text)
            {
                label1.Text = "Introducir Id Completado";
                label1.ForeColor = Color.Lime;

                frmMain.TareasCompletas[3] = true;
                panel2.Enabled = false;
            }
        }
    }
}
